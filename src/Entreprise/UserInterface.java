/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entreprise;

import java.util.regex.*;


/**
 *
 * @author E140928
 */
public class UserInterface {

    public static int setNbEntreprise() {
        int nbEntreprise = -1;
        boolean flag = false;
        do {
            try {
                nbEntreprise = Integer.parseInt(nhpack.Console.readLine("Combien d'entreprise à encoder ? : "));
                if (nbEntreprise <= 0) {
                    System.err.println("Doit etre strictement positif !");
                } else {
                    flag = true;
                }
            } catch (NumberFormatException e) {
                System.err.println("is not a number ! (" + e.getMessage() + ")"); /* Si pas un int */

            }
        } while (!flag);
        return nbEntreprise;
    }

    public static String setEntreprise(String[] szEntreprise) {

        boolean flag = true;
        String element;
        do {
            element = nhpack.Console.readLine();

            if (!Pattern.compile("[a-zA-Z 0-9]*").matcher(element).matches() || element.charAt(0) == ' ') {
                System.err.println("Erreur format");
            } else if (searchEntreprise(szEntreprise, element) != -1) {
                System.err.println("Equipe dejà encodé !");
            } else {
                flag = false;
            }
        } while (flag);
        return element;
    }

    public static String[] setListEntreprise(int szEntreprise) {
        String listeEntreprise[] = new String[szEntreprise];

        for (int i = 0; i < listeEntreprise.length; i++) {
            System.out.print("Entreprise " + (i + 1) + ": ");
            listeEntreprise[i] = setEntreprise(listeEntreprise);
        }
        return listeEntreprise;
    }
    /* Recherche une equipe dans la liste des participants
     * @param String[] list contient les equipes
     *        String element element à rechercher
     * @return int l indice de l element si touvé
     *             -1 si non trouvé
     */

    public static int searchEntreprise(String[] list, String element) {
        int indice = 0;
        while (indice < list.length && list[indice] != null) { /* Si la liste est pas remplie */

            if (list[indice].compareToIgnoreCase(element) == 0) {
                return indice;
            }
            indice++;
        }
        return -1;
    }

    public static float[][] initialisationTabVentes(String[] listeEntreprise) {
        if (listeEntreprise == null) {
            return null;
        } else {
            float tableauVente[][] = new float[listeEntreprise.length][Variation.NB_ANNEE];
            for (int i = 0; i < tableauVente.length; i++) {
                for (int j = 0; j < tableauVente[i].length; j++) {
                    tableauVente[i][j] = 0.0f;
                }
            }
            return tableauVente;
        }
    }

    public static void encoderVenteEntreprise(float szVente[]) {
        for (int i = 0; i < szVente.length; i++) {
            System.out.print("annee " + (i + 1) + ": ");
            szVente[i] = setVente();
        }
    }

    public static float setVente() {
        boolean flag = true;
        float vente = 0;

        do {
            try {
                vente = Float.parseFloat(nhpack.Console.readLine());
                if (vente < 0) {
                    System.out.print("ERREUR ne peut être negatif !\n-> ");
                } else if (vente == 0) {
                    System.err.println("Doit au moins effectuer une vente !");
                } else {
                    flag = false;
                }
            } catch (NumberFormatException e) {
                System.out.print("is not a number ! (" + e.getMessage() + ")\n-> ");
            }
        } while (flag);
        return vente;
    }

    public static void setTableauVente(float[][] szTableauVente, String[] listeEntreprise) {
        for (int i = 0; i < szTableauVente.length; i++) {
            System.out.println(listeEntreprise[i] + ":");
            encoderVenteEntreprise(szTableauVente[i]);
        }
    }

    public static int choixEntreprise(String[] listeEntreprise) {
        boolean flag;
        int choix = 0;
        do {
            flag = false;
            choix = searchEntreprise(listeEntreprise, nhpack.Console.readLine("Recherche: "));
            System.out.println();
            if (choix == -1) {
                System.err.println("Entreprise non trouvée !");
                flag = true;
            }
        } while (flag);

        return choix;
    }

    public static void afficherVariationEntreprise(float variation[]) {
        for (int i = 1; i < variation.length; i++) {
            if (variation[i] == 0.0f) {
                System.out.println("An  " + (i + 1) + ")  " +  variation[i] + " L'entreprise à stagné");
            } else if (variation[i] > 0) {
                System.out.println("An " + (i + 1) + ")  " + variation[i] + " L'entreprise à bien vendu");
            } else {
                System.out.println("An " + (i + 1) + ")  " + variation[i] + " L'entreprise à mal vendu");
            }
        }        
    }
    public static void afficherMoyenneEntreprise(float moyenne[], String[] listeEntreprise){
        for( int i=0; i<moyenne.length; i++){
            if(moyenne[i]>0){
                System.out.println(listeEntreprise[i] + " . " + moyenne[i] + "L'entreprise à bien vendu");
            }else if (moyenne[i]<0){
                 System.out.println(listeEntreprise[i] + " . " + moyenne[i] + " L'entreprise à mal vendu");
            }else{
                 System.out.println(listeEntreprise[i] + " . " + moyenne[i] + "L'entreprise à stagné");
            }
        }
    } 
}
