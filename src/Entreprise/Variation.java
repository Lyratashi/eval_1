/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entreprise;

/**
 *
 * @author E140928
 */
public class Variation {

    public final static int QUITTER = 5;
    public final static int NB_ANNEE = 5;

    public static void main(String[] args) {
        run();
    }

    public static void run() {
        String[] listeEntreprise = null;
        float tableauVente[][] = null;
        boolean firstTime = true;
        int choix;

        do {
            if (firstTime) { /* premier tour de boucle */

                afficherMenu(0);
            } else {
                if (listeEntreprise != null) {
                    afficherMenu(listeEntreprise.length);
                }
            }
            System.out.print("-> ");
            if (firstTime) {
                choix = setChoix(2);
                if (choix == 2) {
                    choix = QUITTER;
                }
            } else {
                choix = setChoix(QUITTER);
            }
            switch (choix) {
                case 1: /* Encoder equipe */

                    if (!firstTime) {
                        System.err.println("Cette operation efface les anciennes données !");
                        System.out.println("Etes vous sur de vouloir faire cette operation ?");
                        System.out.print("[1]oui/[2]non -> ");
                        if (setChoix(2) == 2) {
                            break; /* On ne change rien */

                        }
                    }
                    listeEntreprise = UserInterface.setListEntreprise(UserInterface.setNbEntreprise());
                    if ((tableauVente = UserInterface.initialisationTabVentes(listeEntreprise)) == null) {
                        System.err.println("Une erreur c est produite !");
                        choix = QUITTER;
                    }
                    break;

                case 2:
                    UserInterface.setTableauVente(tableauVente, listeEntreprise);
                    break;
                case 3:
                    if (tableauVente[0][0] == 0) {
                        System.out.println("Option indisponible !");
                        break;
                    }
                    int entreprise = UserInterface.choixEntreprise(listeEntreprise);

                    System.out.println("Variation de l entreprise " + listeEntreprise[entreprise]);
                    UserInterface.afficherVariationEntreprise(Statistiques.calculerVariations(tableauVente[entreprise]));
                    break;
                case 4:
                    if (tableauVente[0][0] == 0) {
                        System.out.println("Option indisponible");
                        break;
                    }
                   UserInterface.afficherMoyenneEntreprise(Statistiques.calculerVariationsMoyennes(tableauVente), listeEntreprise);
                    break;
                default: System.out.println("Merci aurevoir !");
            }
            firstTime = false;
        } while (choix != QUITTER); /* On quitte */

    }

    public static void afficherMenu(int szEquipe) {
        System.out.println();
        System.out.printf("     MENU PRINCIPAL      \nEntreprise: variation//moyenne eval_1\n\n");
        System.out.println("(1) Encoder les entreprises [" + szEquipe + "]");
        if (szEquipe > 0) {
            System.out.println("(2) Encoder les ventes de chaque entreprise");
            System.out.println("(3) Calculer les Taux de variation d'une entreprise");
            System.out.println("(4) Calculer le taux de variation moyen de chaque entreprise");
            System.out.println("(" + QUITTER + ") Quitter ");
        } else {
            System.out.printf("(2) Quitter\n");
        }
    }
    /* Recupere le choix de l user
     * @param int quitter dernier choix possible
     *                          que l user fais un choix
     * @return int choix de l user
     */

    public static int setChoix(int quitter) {
        boolean flag = true;
        int choix = 0;

        do {
            try {
                choix = Integer.parseInt(nhpack.Console.readLine());
                if (choix <= 0 || choix > quitter) {
                    System.out.print("Choix incorrect !\n-> ");
                } else {
                    flag = false;
                }
            } catch (NumberFormatException e) {
                System.out.print("is not a number ! (" + e.getMessage() + ")\n-> ");
            }
        } while (flag);
        return choix;
    }
}
