/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entreprise;

/**
 *
 * @author E140928
 */
public class Statistiques {


    public static float[] calculerVariations(float[] ventes) {
        float resultatVariation[] = new float[Variation.NB_ANNEE]; /* on a pas besoin du premier */

        float base = ventes[0];
        resultatVariation[0]=0;
        for (int i = 1; i < ventes.length; i++) {
            resultatVariation[i] = (ventes[i] - base) / base;
        }
        return resultatVariation;
    }

    public static float[] calculerVariationsMoyennes(float[][] ventes) {
        float moyenne[] = new float[ventes.length]; /* taille 1 dim  = nb entreprise*/
        for (int i = 0; i < moyenne.length; i++) {
            moyenne[i] = sommeVariation(calculerVariations(ventes[i]))/(Variation.NB_ANNEE-1);        
        }
        return moyenne;
    }

    public static float sommeVariation(float variation[]) {
        float szSomme = 0;
        for (float v : variation) {
            szSomme += v;
        }
        return szSomme;
    }
}
